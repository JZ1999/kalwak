import Vue from "vue";
import App from "./App.vue";
import router from "./router/router";
import store from "./store/store";
import axios from "axios";
import vueLogger from "vuejs-logger";
import VueSession from "vue-session";
import "../node_modules/swiper/dist/js/swiper.min.js";
import "./assets/css/main.scss";
import i18n from "./i18n"
Vue.use(require("vue-cookies"));



// vuejs logger and an object for configuration
const isProduction = process.env.NODE_ENV === "production";
Vue.use(vueLogger, {
  isEnabled: true,
  logLevel: isProduction? "error" : "debug",
  stringifyArguments: false,
  showMethodName: true,
  separator: "|",
  showConsoleColors: true,
  showLogLevel: true,
});

// VueJS configs
Vue.config.productionTip = false;
Vue.prototype.$axios = axios;
Vue.use(VueSession);


// set axios Accept-Language header
let language = window.localStorage.getItem("accept-language")
let defaultLang = "en"
axios.defaults.headers.common["Accept-Language"] = language ? language : defaultLang

// save default language to localStorage if only the value from localStorage is undefined
if (!language) {
  window.localStorage.setItem("accept-language", defaultLang)
}

new Vue({
  router,
  store,
  i18n,

  // render: h => h(App),
  render: createElement => createElement(App)
}).$mount("#app");
// If using production node then disable vue devtools
Vue.config.devtools = process.env.NODE_ENV !== "production";


window.$ = window.jQuery = require("jquery");
let csrf_cookie = window.$cookies.get("csrftoken");
axios.defaults.headers.common["X-CSRFToken"] = csrf_cookie; // for all requests
