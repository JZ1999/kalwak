import Vue from 'vue';
import Router from 'vue-router';
import Meta from 'vue-meta'
Vue.use(Router);
Vue.use(Meta);



export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  // when going to a new page, the page will start at top
  scrollBehavior () {
    return { x: 0, y: 0 };
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../views/Home.vue'),
    },
    {
      path: '/projects',
      name: 'projects',
      component: () => import('../views/Projects.vue'),
      children: [
        {
          path: ':category',
          name: 'category',
          component: () => import('../views/sub-views/Category.vue'),
        }
      ]
    },
    {
      path: '/about-us',
      name: 'about-us',
      component: () => import('../views/AboutUs.vue'),
    },
    {
      path: '/services',
      name: 'services',
      component: () => import('../views/Services.vue'),

      children: [
        {
          path: ':service',
          component() {
            return import('../views/sub-views/Service.vue');
          },
        }
      ],
    },
    {
      path: '/hire-us',
      name: 'hire us',
      component: () => import('../views/HireUs.vue'),
    },
    {
      path: '/blog',
      component: () => import('../views/Blog.vue'),
      children: [
        {
          path: '',
          name: 'blog',
          component: () => import('../views/sub-views/BlogDefault.vue'),
        },
        {
          path: ':category/(page/)?:number(\\d+)?',
          name: 'blog list',
          component: () => import('../views/sub-views/BlogCardsList.vue'),
        },
        {
          path: ':category/post/:id(\\d+)',
          nane: 'article',
          component: () => import('../views/sub-views/Post.vue'),
        },
      ],
    },
    {
      path: '*',
      redirect: '/not-found'
    },
    {
      path: '/not-found',
      name: 'not found',
      component: () => import('../views/NotFound.vue'),
    },
    {
      path: '/blog.editor',
      name: 'blog editor',
      component: () => import('../views/BlogEditor.vue'),
    },
    {
      path: '/auth/login',
      name: 'login',
      component: () => import('../views/Login.vue')
    }
  ]
});
// component: () => import(/* webpackChunkName: "about" */ './views/Component.vue')