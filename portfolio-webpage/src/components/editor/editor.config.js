import Header from '@editorjs/header';
import List from '@editorjs/list';
import Delimiter from '@editorjs/delimiter';
import Code from '@editorjs/code';
import Inline from '@editorjs/inline-code';
import Image from '@editorjs/image';
import Embed from '@editorjs/embed'


export default {
  holder: 'editor-holder',
  autofocus: true,
  initialBlock: null,
  placeholder: 'Toque aqui para editar',

  tools: {
    header: {
      class: Header,
      inlineToolbar: ['italic', 'bold'],
      config: {
        placeholder: 'Titulo',
      }
    },

    list: {
      class: List,
      inlineToolbar: true,
    },

    delimiter: {
      class: Delimiter,
    },

    code: Code,

    embed: {
      class: Embed,
      inlineToolbar: true,
      config: {
        services: {
          youtube: true,
          coub: true,
          codepen: {
            regex: /https?:\/\/codepen.io\/([^\/\?\&]*)\/pen\/([^\/\?\&]*)/,
            embedUrl: 'https://codepen.io/<%= remote_id %>?height=300&theme-id=0&default-tab=css,result&embed-version=2',
            html: "<iframe height='300' scrolling='no' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'></iframe>",
            height: 300,
            width: 600,
            id: (groups) => groups.join('/embed/')
          }
        }
      }
    },

    inlineCode: Inline,

    image: {
      class: Image,
      inlineToolbar: true,
      config: {
        endpoints: {
          byFile: process.env.VUE_APP_API_ENDPOINT + '/api/blog/image-upload/',
        }
      }
    }
  },
};