import axios from 'axios';
const API_URL = process.env.VUE_APP_API_ENDPOINT;

// Deals with GET methods to the Projects DRF in the backend
export class ProjectService {

    constructor() {
    }

    // Obtains a json response using the Axios GET method.
    // The information provided is a list of json objects of all
    // the projects in the backend database
    getProjects() {
        const url = API_URL + `/api/project/`;
        return axios.get(url).then(response => response.data);
    }

    // Same as getProjects but gives a single project given a
    // unique primary key as the parameter
    getProject(pk) {
        const url = API_URL + `/api/project/${pk}/`;
        return axios.get(url).then(response => response.data);
    }
}