import axios from 'axios'


function uploadPost(postData = {}, token = '') {
  return new Promise((resolve, reject) => {
    let { title, author, text, tag, thumbnail, lan} = postData
    const postApiUrl = `${process.env.VUE_APP_API_ENDPOINT}/api/blog/`
  
    // create a new FormData instance to send it as multipart/form-data
    const postFormData = new FormData()
  
    // add the required fields for the backend API
    postFormData.append('title', title)
    postFormData.append('author', author)
    postFormData.append('tag', tag)
    postFormData.append('text', text)
    postFormData.append('thumbnail', thumbnail)
    postFormData.append('lan', lan)

    axios.post(postApiUrl, postFormData, {
      headers: {
        'Authorization': `JWT ${token}`
      }
    })
      .then(response => resolve(response))
      .catch(errors => reject(errors))
  })
}


export default uploadPost