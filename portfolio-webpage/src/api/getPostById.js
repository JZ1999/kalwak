import axios from 'axios'

// TODO needs documentation
function getPostById(postId) {
  return new Promise((resolve, reject) => {
    const postApiUrl = `${process.env.VUE_APP_API_ENDPOINT}/api/blog/${postId}/`
    axios.get(postApiUrl)
      .then(response => {
        let post = response.data
        resolve(post)
      })
      .catch(errors => {
        reject(errors)
      })
  })
}


export default getPostById