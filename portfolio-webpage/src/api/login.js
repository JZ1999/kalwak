import axios from 'axios';
// when rejecting for things like, empty username or password
// i'll use 'BAD_CREDENTIALS' string


export default function({ username, password }, recaptchakey) {
  return new Promise((resolve, reject) => {
    
    if (!username || !password || !recaptchakey) return reject('BAD_CREDENTIALS');
    let url = process.env.VUE_APP_API_ENDPOINT + '/api/auth/';

    axios.post(url, {
      username,
      password,
      recaptchakey
    })
      .then(response => {
        let { token } = response.data;
        resolve(token)
      })
      .catch(errors => {
        reject(errors)
      });
  });
}