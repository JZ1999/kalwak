import axios from 'axios'


function sendServiceRequest(clientInformation, recaptchaKey) {
  return new Promise((resolve, reject) => {
    const serviceRequestUrl = `${process.env.VUE_APP_API_ENDPOINT}/api/service_request/`;
    const { firstName, lastName, email, telephone, description, services, files } = clientInformation
    const fullName = firstName + ' ' + lastName

    const clientFormData = new FormData();
    clientFormData.append('name', fullName);
    clientFormData.append('email', email);
    clientFormData.append('telephone', telephone);
    clientFormData.append('description', description);
    clientFormData.append('services', services);
    clientFormData.append('recaptcha_key', recaptchaKey);

    // adding files with the same name
    let index = 0;
    for (index; index < files.length; index++) {
      clientFormData.append('files', files[index])
    }

    // posting the data to the backend
    axios.post(serviceRequestUrl, clientFormData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
      .then(response => resolve(response.data))
      .catch(error => reject(error))
  })
}


export default sendServiceRequest;