import axios from "axios"


function getBlogListByPage(pageNumber) {
  return new Promise((resolve, reject) => {
    const apiUrl = process.env.VUE_APP_API_ENDPOINT + "/api/blog-list?page=" + pageNumber

    axios
      .get(apiUrl)
        .then((response) => {
          let { results } = response.data
          resolve(results)
        })
        .catch((error) => {
          reject(error)
        })
  })
}


export default getBlogListByPage