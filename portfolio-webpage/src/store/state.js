export default {
  language: "",
  projectsInformation: [],
  projects: {
      projectsCategory: "",
      projects: [],
      currentProject: {},
      currentProjectId: 0,
  },
  userIp: "",
  onSearching: false,
  searchText: "",
  filter: {
      year: "",
      month: "",
  },
}