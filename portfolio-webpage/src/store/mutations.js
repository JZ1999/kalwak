export default  {
  setCategory: (state, category) => state.projects.projectsCategory = category,
  setProjects: (state, projects) => state.projects.projects = projects,
  setCurrentProject: (state, project) => state.projects.currentProject = project,
  setAllProjects: (state, projects) => state.projectsInformation = projects,
  setUserIp: (state, ip) => state.userIp = ip,
  setOnSearching: (state, status) => state.onSearching = status, 
  setSearchText: (state, text) => state.searchText = text,
  setYear: (state, year) => state.filter.year = year,
  setMonth: (state, month) => state.filter.month = month,
  setLanguage: (state, language) => state.language = language
}