import axios from "axios"


function setProjectsByCategory({commit, state}) {
  let category = state.projects.projectsCategory;
  let categorizedProjects = [];
  let projects = state.projectsInformation;

  // going through every project object
  // and check if the current page category matches with theirs
  projects.forEach(project => {
    if (project.categories.indexOf(category) > -1) {
      project.pageId = categorizedProjects.length + 1;
      categorizedProjects.push(project);
    }
  });

  if (categorizedProjects) {
    // setting projects.projects to the categorized projects
    commit("setProjects", categorizedProjects);
    // set emptyMatch to false to say that we have not empty matches
  }
}

function getUserIp({ commit }) {
  let getApiUrlEndpoint = `${process.env.VUE_APP_API_ENDPOINT}/api/get-ip/`;
  axios.get(getApiUrlEndpoint)
    .then( response => {
      let  ip = response.data.ip;
      commit("setUserIp", ip);
    })
    .catch( errors => {
      console.error(errors);
    });
}

function changeLanguage({ commit }, language) {
  commit("setLanguage", language)
  axios.defaults.headers.common["Accept-Language"] = language
  window.localStorage.setItem("accept-language", language)
}


export default {
  setProjectsByCategory,
  getUserIp,
  changeLanguage
}