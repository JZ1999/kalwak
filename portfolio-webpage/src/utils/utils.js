function getRangeOfNumbers(min, limit) {
  let numbers = [];
  let number = min;
  for (; number <= limit; number++) {
    numbers.push(number);
  }

  return numbers;
}

function formatMonth(month, locale) {
  let monthsEn = ["Jan","Feb","Mar","Apr","May","June","July","Aug","Sept","Oct","Nov","Dec"];
  let monthsEs = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sept", "Oct", "Nov", "Dic"];

  if (!monthsEs.includes(month) && !monthsEn.includes(month)) return false;
  month = locale === "es" ? monthsEs.indexOf(month) + 1 : monthsEn.indexOf(month) + 1;
  month = month.toString().length === 1 ? '0' + month : month.toString();

  return month;
}

function formatPostDate(date, locale) { // date format should be -> yy-mm-dd
  const monthsEs = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
  const monthsEn = ['January', 'February', 'March', 'APril', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'Dicember'];
  const datePieces = date.split('-');
  let [year, month, day] = datePieces;

  month = month.split('');
  let monthIndex = Number((month[0] === '0' ? month[1] : month.join(''))) - 1;
  month = locale === "es" ? monthsEs[monthIndex] : monthsEn[monthIndex];

  day = day.split('');
  day = day[0] === '0' ? day[1] : day.join('')

  let postDate = locale === "es" ? `Publicado el ${day} de ${month} del ${year}` : `Published on ${month} ${day}, ${year}`;
  return  postDate;

}

function getErrorsMessageFromErrorsOject(errorsObject) {
  let errorsMessage = '';
  let errorsValues = Object.values(errorsObject)

  for (let index = 0; index < errorsValues.length; index++) {
    let error = errorsValues[index]
    errorsMessage += `- ${error}\n`
  }

  return errorsMessage;
}

function saveAcceptLanguageToLocalStorage(language) {
  window.localStorage.setItem("accept-language", language)
  window.location.reload()
}

function requireCategoryByNameAndFolderLanguage(category, languageFolder) {

  return require(`@/assets/images/categories-images/${languageFolder}/${category}`)
}

export {
  getRangeOfNumbers,
  formatMonth,
  formatPostDate,
  getErrorsMessageFromErrorsOject,
  saveAcceptLanguageToLocalStorage,
  requireCategoryByNameAndFolderLanguage}