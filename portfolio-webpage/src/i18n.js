import Vue from "vue"
import VueI18n from "vue-i18n"
Vue.use(VueI18n)


function loadLocaleMessages () {
  const locales = require.context("./languages", true, /[A-Za-z0-9-_,\s]+\.json$/i)
  const messages = {}
  locales.keys().forEach(key => {
    const matched = key.match(/([A-Za-z0-9-_]+)\./i)
    if (matched && matched.length > 1) {
      const locale = matched[1]
      messages[locale] = locales(key)
    }
  })
  return messages
}


let language = window.localStorage.getItem("accept-language")
let userLang = navigator.language || navigator.userLanguage;

// Sets userLang to 'es' or 'en' depending on the default language of the user
// For example turns 'en-US' into 'en' and 'es-MX' into 'es' and if the user uses
// a different language from 'es' or 'en' then defaults to 'en'
if(userLang.includes('es')){
  userLang = 'es';
}else if(userLang.includes('en')){
  userLang = 'en';
}else{
  userLang = 'en';
}

document.documentElement.setAttribute("lang", language ? language : userLang)


export default new VueI18n({
  locale: language ? language : userLang,
  fallbackLocale: userLang,
  messages: loadLocaleMessages() // []
})