# Documentation is done by vuese

## To generate the docs for every component, run:

``` 
npm run docs:gen
```

## To serve a localhost to see components docs, run:

```
npm run docs:serve
```

Note: These commands have to be put in the project root directory
Note: this documentation assumes you've already made installations with `npm install` command