# portafolio-website

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Generate documentation
To generate documentation run this command
```
npm run docs:gen
```
To view it in a npm server
```
npm run docs:serve
```


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
