module.exports = {
  teamMembersEndpoint: process.env.VUE_APP_TEAM_MEMBERS_INFORMATION_ENDPOINT,
  api_endpoint: process.env.VUE_APP_API_ENDPOINT,
  recaptcha_key: process.env.VUE_APP_RECAPTCHA_KEY
}
