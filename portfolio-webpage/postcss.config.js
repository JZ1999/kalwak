const purgecss = require('@fullhuman/postcss-purgecss')


module.exports = {
  plugins: [
    purgecss({
      content: ['./src/**/*.html', './src/**/*.js', './src/**/*.html', './src/**/*.vue'],
      css: ["./src/**/*.scss"]
    })
  ]
}
