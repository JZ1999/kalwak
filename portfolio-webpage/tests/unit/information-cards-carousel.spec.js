import { mount } from "@vue/test-utils"
import Vue from "vue"
import InformationCardsCarousel from "../../src/components/sub-layout/information-cards-carousel.vue"

window.matchMedia = (query) => ({ matches: false, media: query })

// needed component values
const cards = [
  {
    title: "card title",
    description: "card description",
    imageSrc: "information.svg"
  }
]

describe("InformationCardsCarousel", () => {
  // wrapper.vm === the mounted InformationCardsCarousel instance
  const wrapper = mount(InformationCardsCarousel, {
    mocks: {
      $t: () => "fake translated text"
    },
    propsData: {
      cards
    }
  })

  it("renders a div (with class information-section__content-card) for every card in cards prop", () => {
    const cardsLen = cards.length
    const cardsElements = wrapper.findAll(".information-section__content-card")

    expect(cardsElements.length).toBe(cardsLen)
  })

  it("setDotsBasedOnScreenType assigns an empty array to carouselDots if screenType is large", () => {
    wrapper.vm.$data.screenType = "large"
    wrapper.vm.setDotsBasedOnScreenType()
    expect(wrapper.vm.$data.carouselDots).toEqual([])
  })

  it("setDotsBasedOnScreenType assigns an array [0, 1] to carouselDots if screenType is medium", () => {
    wrapper.vm.$data.screenType = "medium"
    wrapper.vm.setDotsBasedOnScreenType()
    expect(wrapper.vm.$data.carouselDots).toEqual([0, 1])
  })

  it("setDotsBasedOnScreenType assigns an array [0, 1, 2, 3] to carouselDots if screenType is small", () => {
    wrapper.vm.$data.screenType = "small"
    wrapper.vm.setDotsBasedOnScreenType()
    expect(wrapper.vm.$data.carouselDots).toEqual([0, 1, 2, 3])
    wrapper.vm.moveCarouselWrapper(1)
  })

  it("moveCarouselWrapper moves carousel wrapper by applying a negative translation in the X axis", async () => {
    wrapper.vm.moveCarouselWrapper(1) // move carousel wrapper -100% in the X axis
    
    await Vue.nextTick()
    const carouselWrapper = wrapper.find(".information-section__content-cards-wrapper")
    expect(carouselWrapper.attributes("style")).toBe("transform: translateX(-100%);")
  })

  it("getCarouselPositonFromIndex returns a valid carousel position based on the index passed as argument, example index = 0, returned value should be 0%, index = 1, returned value should be 100%", () => {
    let index = 1
    let carouselPositon = wrapper.vm.getCarouselPositonFromIndex(index)
    expect(carouselPositon).toBe("100%")

    index = 0
    carouselPositon = wrapper.vm.getCarouselPositonFromIndex(index)
    expect(carouselPositon).toBe("0%")

    index = 3
    carouselPositon = wrapper.vm.getCarouselPositonFromIndex(index)
    expect(carouselPositon).toBe("300%")
  })
})
