import * as utils from '../../../src/utils/utils'


describe('Utility functions', () => {
  it('getRangeOfNumbers should return an array of numbers from min to limit interval, which are its parameters', () => {
    let rangeOfNumbers = utils.getRangeOfNumbers(1, 5);

    expect(rangeOfNumbers).toEqual([1, 2, 3, 4, 5]);
  })

  it('formatMonth should return a formatted month index string ... example Ene = 01, Dec = 12, if the month index is not a two length number, the index if prefix with a zero', () => {
    let formattedMonth = utils.formatMonth('Ene', 'es');

    expect(formattedMonth).toBe('01');
    expect(utils.formatMonth('Dic', 'es')).toBe('12');

    // if month is not a valid month name (actually a month in short format Ene for Enero etc..)
    // it should return false
    expect(utils.formatMonth('Not even a month')).toBeFalsy()
  })

  it('formatPostDate should return a more good-looking formatted date base on a date string in format yy-mm-dd', () => {
    let date = '2019-11-26';

    expect(utils.formatPostDate(date, 'es')).toBe('Publicado el 26 de Noviembre del 2019');
  })

  it('getErrorsMessageFromErrorsOject should return a string with the values of the errorsObject properties', () => {
    let errors = {
      name: 'name',
      password: 'password'
    }

    let errorsMessage = utils.getErrorsMessageFromErrorsOject(errors)
    expect(errorsMessage).toBe('- name\n- password\n')
    /*
      - name
      - password
    */
  })
})