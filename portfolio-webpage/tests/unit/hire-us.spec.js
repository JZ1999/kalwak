import HireUs from "../../src/views/HireUs.vue";
import { createLocalVue, shallowMount } from "@vue/test-utils";
// import { render } from "@vue/server-test-utils";
import Vuei18n from "vue-i18n";

describe("hire-us", () => {
  const localVue = createLocalVue();
  localVue.use(require("vue-cookies"));
  localVue.use(Vuei18n)

  const i18n = new Vuei18n({
    locale: "en",
    messages: {
      en: {
        "hireUs": {
          "title": "Would you like to work with us?",
          "subtitle": "Fill in the following form to contact our specialist",
          "form": {
            "name": "Name",
            "lastname": "Last Name",
            "phonenumber": "Phone Number",
            "email": "E-mail",
            "servicePicker": {
              "title": "Services",
              "services": [
                {
                  "id": 1,
                  "name": "Static Website"
                },
                {
                  "id": 2,
                  "name": "Dynamic Website"
                },
                {
                  "id": 3,
                  "name": "Website with Secure Payments"
                },
                {
                  "id": 4,
                  "name": "Website Graphic Design"
                },
                {
                  "id": 5,
                  "name": "Logo"
                },
                {
                  "id": 6,
                  "name": "Inventory System"
                }
              ],
              "note": "Each web site we make, we create with scalability, robustness and security standards that will be included in the estimate project price."
            },
            "enquiry": "Enquiry",
            "messagePlaceholder": "Tell us about your project...",
            "files": {
              "attachFiles": "Attach Files",
              "numOfFiles": "Number of files",
              "maxFileSize": "Maximum size of files"
            },
            "requestService": "Request Service"
          }
        },
      },
      es: {
        "hireUs": {
          "title": "¿Le gustaría trabajar con nosotros?",
          "subtitle": "Complete el siguiente formulario para ser ascesorado",
          "form": {
            "name": "Nombre",
            "lastname": "Apellido",
            "phonenumber": "Teléfono",
            "email": "E-mail",
            "servicePicker": {
              "title": "Servicio a consultar",
              "services": [
                {
                  "id": 1,
                  "name": "Sitio Estático"
                },
                {
                  "id": 2,
                  "name": "Sitio Dinámico"
                },
                {
                  "id": 3,
                  "name": "Sitio web con Pagos Seguros"
                },
                {
                  "id": 4,
                  "name": "Diseño Gráfico de una Página Web"
                },
                {
                  "id": 5,
                  "name": "Logo"
                },
                {
                  "id": 6,
                  "name": "Sistema de Inventario"
                }
              ],
              "note": "Cada sitio web que hacemos, lo creamos con estándares de escalabilidad, robustez y de seguridad el cual viene ya incluido en la cotización de cualquier sitio web."
            },
            "enquiry": "Consulta",
            "messagePlaceholder": "Cuentenos de su proyecto...",
            "files": {
              "attachFiles": "Adjuntar Archivos",
              "numOfFiles": "Número de archivos",
              "maxFileSize": "Tamaño maximo de archivos hasta"
            },
            "requestService": "Cotizar"
          }
        },
      }
    }
  })
  let wrapper = shallowMount(HireUs, {
    localVue,
    i18n,
    mocks: {
      $t: () => "some text to translate"
    }
  });

  // data function
  it("you have a data function and that data function should return an object", () => {
    HireUs.$i18n = {
      locale: "en",
      messages: {
        "en": {
          "hireUs": {
            "form": {
              "notification": ""
            }
          }
        }
      }
    }
    expect(typeof HireUs.data).toBe("function");
    expect(typeof HireUs.data()).toBe("object");
  });

  // computed properties
  it("buttonDisabledByInputs computed property should return true if required fields are empty/undefined", () => {
    wrapper.setData({
      serviceRequest: {
        firstName: "",
        lastName: "",
        email: "",
        description: "",
        telephone: "",
        files: [],
        services: []
      }
    });

    expect(wrapper.vm.buttonDisabledByInputs).toBe(true);

    // now with undefined values instead of empty strings
    wrapper.setData({
      serviceRequest: {
        firstName: undefined,
        lastName: undefined,
        email: undefined,
        description: undefined,
        telephone: undefined,
        files: [],
        services: []
      }
    });

    expect(wrapper.vm.buttonDisabledByInputs).toBe(true);
  });

  it("buttonDisabledByInputs computed property should return true if required fields are not empty/undefined", () => {
    wrapper.setData({
      serviceRequest: {
        firstName: "John",
        lastName: "Doe",
        email: "johndoe@email.com",
        description: "Hello Kalwak",
        telephone: "+123456789",
        files: [],
        services: []
      },
      recaptchaKey: "asdfghjl"
    });

    expect(wrapper.vm.buttonDisabledByInputs).toBe(false);
  });

  it("buttonDisabled should return true if buttonDisabledByInputs is true, else if buttonDisabledByInputs is false returns false", () => {
    wrapper.setData({
      serviceRequest: {
        firstName: "",
        lastName: "",
        email: "",
        description: "",
        telephone: "",
        files: [],
        services: []
      }
    });

    expect(wrapper.vm.buttonDisabled).toBe(true);

    wrapper.setData({
      serviceRequest: {
        firstName: "John",
        lastName: "Doe",
        email: "johndoe@email.com",
        description: "Hello Kalwak",
        telephone: "+123456789",
        files: [],
        services: []
      },
      recaptchaKey: "asdfghjl"
    });

    expect(wrapper.vm.buttonDisabled).toBe(false);

    wrapper.setData({
      serviceRequest: {
        firstName: "John",
        lastName: "Doe",
        email: "johndoe@email.com",
        description: "Hello Kalwak",
        telephone: "+123456789",
        files: [],
        services: []
      },
      recaptchaKey: ""
    });

    expect(wrapper.vm.buttonDisabled).toBe(true);
  });
});
