import tempfile
from datetime import date

from PIL import Image
from django.test import TestCase, override_settings
from rest_framework.test import APIClient
from rest_framework import status
from Blog.models import Blog
from django.utils import timezone


@override_settings(UPDATE_RSS=False)
class BlogTestCase(TestCase):
    tmp_file = None
    url = "/api/blog-list/?page=1"

    def setUp(self) -> None:
        self.client = APIClient()
        image = Image.new('RGB', (100, 100))

        self.tmp_file = tempfile.NamedTemporaryFile(suffix='.jpg')
        image.save(self.tmp_file)
        self.tmp_file.seek(0)

        x = Blog.objects.create(title="Blog1", author="Joseph", tag="development", date=date(2020, 5, 9),
                                text="aaa")
        x.thumbnail.save(name="tmp.jpg", content=self.tmp_file)
        x = Blog.objects.create(title="Blog11", author="Joseph", tag="development", date=date(2020, 5, 9),
                                text="aaa")
        x.thumbnail.save(name="tmp.jpg", content=self.tmp_file)
        x = Blog.objects.create(title="Blog12", author="Joseph", tag="development", date=date(2020, 5, 9),
                                text="aaa")
        x.thumbnail.save(name="tmp.jpg", content=self.tmp_file)
        x = Blog.objects.create(title="Blog13", author="Joseph", tag="development", date=date(2020, 5, 9),
                                text="aaa")
        x.thumbnail.save(name="tmp.jpg", content=self.tmp_file)
        x = Blog.objects.create(title="Blog14", author="Joseph", tag="development", date=date(2020, 5, 9),
                                text="aaa")
        x.thumbnail.save(name="tmp.jpg", content=self.tmp_file)
        x = Blog.objects.create(title="Blog2", author="Joseph", tag="development", date=date(2019, 6, 20),
                                text="bbb")
        x.thumbnail.save(name="tmp.jpg", content=self.tmp_file)
        x = Blog.objects.create(title="Blog3", author="Joseph", tag="development", date=date(2019, 8, 19),
                                text="ccc")
        x.thumbnail.save(name="tmp.jpg", content=self.tmp_file)
        x = Blog.objects.create(title="Blog4", author="Joseph2", tag="startup", date=date(2019, 6, 20),
                                text="ddd")
        x.thumbnail.save(name="tmp.jpg", content=self.tmp_file)
        Blog.objects.create(title="Blog5", author="Joseph3", tag="design", date=date(2019, 7, 30),
                            text="eee")
        x.thumbnail.save(name="tmp.jpg", content=self.tmp_file)
        x = Blog.objects.create(title="Blog6", author="Joseph4", tag="security", date=date(2019, 8, 22),
                                text="fff")
        x.thumbnail.save(name="tmp.jpg", content=self.tmp_file)
        x = Blog.objects.create(title="Blog7", author="Joseph5", tag="social", date=date(2019, 8, 15),
                                text="ggg")
        x.thumbnail.save(name="tmp.jpg", content=self.tmp_file)

    def test_get_month1(self):
        url = self.url + "&month=05"

        response = self.client.get(url)

        filtering = len(Blog.objects.filter(date__month=5)[:6])
        self.assertEqual(filtering, len(response.data["results"]))

    def test_get_month2(self):
        url = self.url + "&month=01"

        response = self.client.get(url)

        filtering = len(Blog.objects.filter(date__month=1)[:6])
        self.assertEqual(filtering, len(response.data["results"]))

    def test_get_tag1(self):
        url = self.url + "&tag=development"

        response = self.client.get(url)

        filtering = len(Blog.objects.filter(tag="development")[:6])
        self.assertEqual(filtering, len(response.data["results"]))

    def test_get_tag2(self):
        url = self.url + "&tag=security"

        response = self.client.get(url)

        filtering = len(Blog.objects.filter(tag="security")[:6])
        self.assertEqual(filtering, len(response.data["results"]))

    def test_get_tag3(self):
        url = "/api/blog-list/?page=2&tag=development"

        response = self.client.get(url)

        expected = 1
        self.assertEqual(expected, len(response.data["results"]))

    def test_get_year1(self):
        url = self.url + "&year=2020"

        response = self.client.get(url)

        filtering = len(Blog.objects.filter(date__year=2020)[:6])
        self.assertEqual(filtering, len(response.data["results"]))

    def test_get_year2(self):
        url = self.url + "&year=2019"

        response = self.client.get(url)

        filtering = len(Blog.objects.filter(date__year=2019)[:6])
        self.assertEqual(filtering, len(response.data["results"]))

    def test_paginator(self):
        url = self.url

        response = self.client.get(url)

        expected = 6
        self.assertEqual(expected, len(response.data["results"]))

        url = "/api/blog-list/?page=2"
        response = self.client.get(url)
        expected = 5
        self.assertEqual(expected, len(response.data["results"]))

        url = "/api/blog-list/?page=3"
        response = self.client.get(url)
        expected = status.HTTP_404_NOT_FOUND
        self.assertEqual(expected, response.status_code)

    def test_combinations1(self):
        url = self.url + "&month=08&year=2019"

        response = self.client.get(url)

        filtering = len(Blog.objects.filter(date__month=8, date__year=2019))
        self.assertEqual(filtering, len(response.data["results"]))

        url = self.url + "&month=05&year=2020"

        response = self.client.get(url)

        filtering = len(Blog.objects.filter(date__month=5, date__year=2020))
        self.assertEqual(filtering, len(response.data["results"]))

    def test_combinations2(self):
        url = self.url + "&tag=startup&year=2019"

        response = self.client.get(url)

        filtering = len(Blog.objects.filter(tag="startup", date__year=2019))
        self.assertEqual(filtering, len(response.data["results"]))

        url = self.url + "&month=08&tag=social"

        response = self.client.get(url)

        filtering = len(Blog.objects.filter(date__month=8, tag="social"))
        self.assertEqual(filtering, len(response.data["results"]))

    def test_combinations3(self):
        url = self.url + "&tag=design&year=2019&month=07"

        response = self.client.get(url)

        filtering = len(Blog.objects.filter(date__month=7, tag="design", date__year=2019))
        self.assertEqual(filtering, len(response.data["results"]))

        url = self.url + "&month=07&tag=design&year=2020"

        response = self.client.get(url)

        filtering = len(Blog.objects.filter(date__month=7, tag="design", date__year=2020))
        self.assertEqual(filtering, len(response.data["results"]))

    def test_title(self):
        url = self.url + "&title=blog1"

        response = self.client.get(url)

        filtering = len(Blog.objects.filter(title__icontains="Blog1"))
        self.assertEqual(filtering, len(response.data["results"]))

        url = self.url + "&title=blog11"

        response = self.client.get(url)

        filtering = len(Blog.objects.filter(title__icontains="blog11"))
        self.assertEqual(filtering, len(response.data["results"]))

        url = self.url + "&title=blog111"

        response = self.client.get(url)

        filtering = len(Blog.objects.filter(title__icontains="blog111"))
        self.assertEqual(filtering, len(response.data["results"]))
