import tempfile

from PIL import Image
from django.contrib.auth.models import User
from django.test import TestCase, override_settings
from rest_framework.test import APIClient
from rest_framework import status
from Blog.models import Blog
from django.utils import timezone
from rest_framework_jwt.settings import api_settings


@override_settings(UPDATE_RSS=False)
class BlogTestCase(TestCase):
    tmp_file = None

    def setUp(self) -> None:
        user = User.objects.create_user(username="joseph", email="joseph@c.com", password="admin12345")
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        self.client = APIClient()

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + token)
        image = Image.new('RGB', (100, 100))

        self.tmp_file = tempfile.NamedTemporaryFile(suffix='.jpg')
        image.save(self.tmp_file)
        self.tmp_file.seek(0)

    def test_blog_post_successful(self):
        data = {"title": "A Wonderful Test",
                "author": "Joseph",
                "tag": "startup",
                "text": "This is my blog.",
                "thumbnail": self.tmp_file}
        response = self.client.post('/api/blog/', data=data,
                                    format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response.data.pop('date')
        self.assertEqual(response.data["text"], data['text'])

    def test_blog_get_successful(self):
        response = self.client.get('/api/get/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_blog_retrieve_successful(self):
        Blog.objects.create(
            id=1,
            title="Finding the perfect test",
            author="Joseph",
            tag="Startup",
            text="TEST TEXT"
        )
        path = '/api/blog/1/'
        response = self.client.get(path)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_blog_post_missing_title_field(self):
        data = {"author": "Joseph",
                "tag": "startup",
                "text": "This is my blog.",
                "thumbnail": self.tmp_file}
        response = self.client.post('/api/blog/', data=data,
                                    format='multipart')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        correct_response = b'{"title":["This field is required."]}'
        self.assertEqual(response.content, correct_response)

    def test_blog_post_wrong_author(self):
        data = {"title": "Joseph",
                "author": "Test",
                "tag": "startup",
                "text": "This is my blog.",
                "thumbnail": self.tmp_file}
        response = self.client.post('/api/blog/', data=data,
                                    format='multipart')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        correct_response = b'{"author":["\\"Test\\" is not a valid choice."]}'
        self.assertEqual(response.content, correct_response)

    def test_blog_post_missing_text_field(self):
        data = {"title": "Test Title",
                "author": "Joseph",
                "tag": "startup",
                "thumbnail": self.tmp_file}
        response = self.client.post('/api/blog/', data=data,
                                    format='multipart')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        correct_response = b'{"text":["This field is required."]}'
        self.assertEqual(response.content, correct_response)

    def test_blog_post_missing_tag_field(self):
        data = {"title": "A Wonderful Test",
                "author": "Joseph",
                "text": "This is my blog.",
                "thumbnail": self.tmp_file}
        response = self.client.post('/api/blog/', data=data,
                                    format='multipart')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        correct_response = b'{"tag":["This field is required."]}'
        self.assertEqual(response.content, correct_response)

    def test_blog_post_date_added(self):
        data = {"title": "A Wonderful Test",
                "author": "Joseph",
                "tag": "startup",
                "text": "This is my blog.",
                "date": "01/01/2019",
                "thumbnail": self.tmp_file}
        response = self.client.post('/api/blog/', data=data,
                                    format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # The date here must be different from the one sended in the post.
        self.assertEqual(response.data['date'], str(timezone.localdate()))

    def test_blog_image_upload_400(self):
        data = {
            "image": ""
        }

        response = self.client.post('/api/blog/image-upload/', data=data,
                                    format='multipart')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_blog_image_upload_200(self):
        data = {
            "image": self.tmp_file
        }

        response = self.client.post('/api/blog/image-upload/', data=data,
                                    format='multipart')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['success'], 1)
        self.assertIn(".jpg", response.data['file']['url'])
        self.assertIn("/assets/blog/images/", response.data['file']['url'])

    def test_blog_post_401(self):
        data = {"title": "A Wonderful Test",
                "author": "Joseph",
                "tag": "startup",
                "text": "This is my blog.",
                "thumbnail": self.tmp_file}
        self.client.credentials()
        response = self.client.post('/api/blog/', data=data,
                                    format='multipart')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        correct_response = b'{"detail":"Authentication credentials were not provided."}'
        self.assertEqual(response.content, correct_response)

    def test_language_filter_es(self):
        Blog.objects.create(
            id=9,
            title="Finding the perfect test",
            author="Joseph",
            tag="Startup",
            text="TEST TEXT",
            lan="es"
        )
        path = '/api/blog/9/'
        response = self.client.get(path)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        headers = {
            "Accept-Language": "es"
        }
        response = self.client.get(path, **headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_language_filter_en(self):
        Blog.objects.create(
            id=9,
            title="Finding the perfect test",
            author="Joseph",
            tag="Startup",
            text="TEST TEXT",
            lan="en"
        )
        path = '/api/blog/9/'
        response = self.client.get(path)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        headers = {
            "Accept-Language": "en"
        }
        response = self.client.get(path, **headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        headers = {
            "Accept-Language": "es"
        }
        response = self.client.get(path, **headers)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
