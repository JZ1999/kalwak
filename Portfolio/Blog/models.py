from django.db import models

from Portfolio import settings


class Blog(models.Model):
    authors = [("Joseph", "Joseph"), ("Jennifer", "Jennifer"), ("Edwin", "Edwin")]
    title = models.CharField(blank=False, max_length=100)
    author = models.CharField(choices=authors, default=authors[0][0], blank=False, max_length=100)
    tags_options = [("development", "Development"), ("social", "Social Media"),
                    ("startup", "Startup"), ("security", "Security"),
                    ("design", "Graphic Design")]
    tag = models.CharField(choices=tags_options, max_length=11)
    date = models.DateField(auto_now_add=True)
    thumbnail = models.ImageField(upload_to='blog')
    text = models.TextField(blank=False, max_length=30000)
    lan = models.CharField(choices=settings.LANGUAGES, default=settings.LANGUAGE_CODE, max_length=2, blank=False)

    def __str__(self):
        return f"{self.title}"


class BlogImage(models.Model):
    image = models.ImageField(upload_to='blog/images/')

    def __str__(self):
        return f"{self.image.name}"
