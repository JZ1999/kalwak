from django.contrib import admin

# Register your models here.
from Blog.models import Blog


class BlogAdmin(admin.ModelAdmin):
    list_filter = ["tag", "date", "lan"]
    save_as = True
    list_display = ("title", "lan")


admin.site.register(Blog, BlogAdmin)
