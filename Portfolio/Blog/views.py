from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets, status
from rest_framework.generics import ListAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from Blog.models import BlogImage
from Blog.serializers import (
    BlogListSerializer,
    BlogFilterSerializer,
    BlogImageSerializer,
)
from Portfolio.filters import LanguageFilter
from .serializers import BlogSerializer
from .models import Blog


class StandardResultsSetPagination(PageNumberPagination):
    """
    A paginator that gives 6 elements at a time
    """

    page_size = 6
    page_size_query_param = "page_size"
    max_page_size = 6


class BlogListView(ListAPIView):
    """
    A paginator view for blogs giving 6 by 6 and with a serializer
    that gives all of the Blogs' fields but the text field, with filters by
    tags, month and year.
    """

    queryset = Blog.objects.all()
    serializer_class = BlogListSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [LanguageFilter]

    def get(self, request, *args, **kwargs):
        params = dict(request.query_params)
        try:
            params.pop("page")
        except KeyError:
            return Response(
                {
                    "detail": "page query parameter was not provided. Must be used like so: /api/blog-list/?page=1"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        for param in params:
            params[param] = "".join(params[param])
        serializer = BlogFilterSerializer(data=params)

        if params:
            if serializer.is_valid():
                if "tag" in serializer.validated_data:
                    self.queryset = self.queryset.filter(
                        tag=serializer.validated_data.get("tag")
                    )
                if "year" in serializer.validated_data:
                    self.queryset = self.queryset.filter(
                        date__year=serializer.validated_data.get("year")
                    )
                if "month" in serializer.validated_data:
                    self.queryset = self.queryset.filter(
                        date__month=serializer.validated_data.get("month")
                    )
                if "title" in serializer.validated_data:
                    self.queryset = self.queryset.filter(
                        title__icontains=serializer.validated_data.get("title")
                    )
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return super(BlogListView, self).list(request, *args, **kwargs)


class BlogView(viewsets.ModelViewSet):
    """
    Blog viewset. It allows you to perform Get and Post methods.
    With GET method it can give you the list of Blogs or you can retrieve
    individual Blogs.
    """

    serializer_class = BlogSerializer
    queryset = Blog.objects.all()
    authentication_classes = []
    permission_classes = []
    filter_backends = [LanguageFilter]

    def get_authenticators(self):
        if "post" in self.action_map.keys():
            self.authentication_classes.append(JSONWebTokenAuthentication)
            self.permission_classes.append(IsAuthenticated)
        else:
            self.authentication_classes = []
            self.permission_classes = []
        return super(BlogView, self).get_authenticators()


# TODO remove permission and authentication classes
class BlogImageUploadView(viewsets.ModelViewSet):
    """
    BlogImage viewset. It allows you to perform the POST method
    to save the image and return a json that may be used by editorjs.
    """

    serializer_class = BlogImageSerializer
    queryset = BlogImage.objects.all()
    permission_classes = ()
    authentication_classes = ()

    @method_decorator(csrf_exempt)
    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            blog_image: BlogImage = serializer.save()
            response = {
                "success": 1,
                "file": {"url": request.build_absolute_uri(blog_image.image.url)},
            }
            return Response(response)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
