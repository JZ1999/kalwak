from datetime import datetime

from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from Blog.models import Blog
from lxml import etree

from Portfolio.settings import BASE_DIR


@receiver(post_save, sender=Blog)
def my_handler(sender, **kwargs):
    if settings.UPDATE_RSS:
        read_rss(sender)


def read_rss(sender):
    try:
        with open(BASE_DIR + '/static_dev/blog-rss.xml', 'r') as rss_xml:
            blog: Blog
            blog = sender.objects.last()

            root = etree.Element("item")

            title = etree.Element("title")
            title.text = blog.title
            link = etree.Element("link")
            link.text = f"https://kalwak.cr/blog/{blog.tag}/post/{str(blog.id)}"
            description = etree.Element("description")
            description.text = f"New blog added by {blog.author} in the category of {blog.tag}."
            author = etree.Element("author")
            author.text = f"{settings.DEFAULT_FROM_EMAIL} ({blog.author})"
            pub_date = etree.Element("pubDate")
            pub_date.text = blog.date.strftime("%a, %d %b %Y %H:%M:%S") + " EST"
            guid = etree.Element("guid", isPermaLink='false')
            guid.text = f"Blog - {str(blog.id)}"
            category = etree.Element("category")
            category.text = blog.tag
            media = etree.Element("media", url="https://kalwak.cr" + blog.thumbnail.url, width="55", height="55")

            root.append(title)
            root.append(link)
            root.append(description)
            root.append(author)
            root.append(pub_date)
            root.append(guid)
            root.append(category)
            root.append(media)
            item_to_string = etree.tostring(root, pretty_print=True).decode('utf-8')
            item_to_string = item_to_string.replace("<media", "<media:thumbnail")

            rss_content = rss_xml.readlines()
            length = len(rss_content)
            rss_content.insert(length - 2, item_to_string)

            # Updating rss build date
            build = etree.Element("lastBuildDate")
            build.text = datetime.now().strftime("%a, %d %b %Y %H:%M:%S")
            build = etree.tostring(build, pretty_print=True).decode('utf-8')
            rss_content.pop(3)
            rss_content.insert(3, build)
            new_rss_content = "".join(rss_content)
    except Exception as e:
        return

    if blog.lan == settings.LANGUAGE_CODE:
        with open(BASE_DIR + '/static_dev/blog-rss.xml', 'w') as rss_xml:
            rss_xml.write(new_rss_content)
