from django.contrib import admin
from .models import *


class GalleryInline(admin.TabularInline):
    model = Gallery


class ProjectAdmin(admin.ModelAdmin):
    list_filter = [
        'categories',
        'date',
        'lan'
    ]
    inlines = [
        GalleryInline
    ]
    save_as = True

    list_display = ["name", "lan"]


class LogsAdmin(admin.ModelAdmin):
    readonly_fields = (
        'datetime', 'status_code', 'status_text', 'response',
        'request', 'ipv4_address', 'path', 'is_ajax', 'is_secure'
    )

    list_filter = (
        'datetime', 'status_code', 'path', 'is_ajax', 'is_secure'
    )


admin.site.register(Logs, LogsAdmin)
admin.site.register(Project, ProjectAdmin)
