from django.apps import AppConfig


class ProjectConfig(AppConfig):
    name = 'Project'


class PhotoConfig(AppConfig):
    name = "Gallery"
