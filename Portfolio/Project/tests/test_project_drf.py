from django.test import TestCase, override_settings
from django.utils import timezone
from rest_framework.test import APIClient
from rest_framework import status
from rest_framework.utils import json

from Project.models import Project


class ProjectTestCase(TestCase):

    def setUp(self) -> None:
        self.client = APIClient()

    def test_404(self):
        path = '/api/project/100/'
        response = self.client.get(path)
        expected = status.HTTP_404_NOT_FOUND
        self.assertEqual(response.status_code, expected)

    def test_405(self):
        data = {
            'name': 'test',
            'cover_page': 'cover_pages/photo.jpg',
            'description': 'ok',
            'website': 'http://kalwak.xyz',
            'date': timezone.now()
        }
        path = '/api/project/'
        response = self.client.post(path, data)
        expected = status.HTTP_405_METHOD_NOT_ALLOWED
        self.assertEqual(response.status_code, expected)

    def test_200(self):
        Project.objects.create(
            name="Test",
            cover_page="cover_page/image.jpg",
            description="ok",
            website="http://kalwak.xyz",
            date=timezone.now(),
            pk=1
        )
        path = '/api/project/1/'
        response = self.client.get(path)
        expected = status.HTTP_200_OK
        self.assertEqual(response.status_code, expected)

    def test_200_content(self):
        Project.objects.create(
            name="Test",
            cover_page="cover_page/image.jpg",
            description="ok",
            website="http://kalwak.xyz",
            date=timezone.now(),
            pk=2
        )
        path = '/api/project/2/'
        response = self.client.get(path)
        expected = status.HTTP_200_OK
        self.assertEqual(response.status_code, expected)
        data = json.loads(response.content)["website"]
        expected = "http://kalwak.xyz"
        self.assertEqual(data, expected)

    def test_200_language_filter_es(self):
        Project.objects.create(
            name="Test",
            cover_page="cover_page/image.jpg",
            description="ok",
            website="http://kalwak.cr",
            date=timezone.now(),
            lan="es",
            pk=3
        )
        headers = {
            'Accept-Language': "es",
        }
        path = '/api/project/3/'
        response = self.client.get(path, **headers)
        expected = status.HTTP_200_OK
        self.assertEqual(response.status_code, expected)
        data = json.loads(response.content)["website"]
        expected = "http://kalwak.cr"
        self.assertEqual(data, expected)

        response = self.client.get(path)
        expected = status.HTTP_404_NOT_FOUND
        self.assertEqual(response.status_code, expected)
        data = json.loads(response.content)
        expected = {'detail': 'Not found.'}
        self.assertEqual(data, expected)

        path = '/api/project/1000/'
        response = self.client.get(path, **headers)
        expected = status.HTTP_404_NOT_FOUND
        self.assertEqual(response.status_code, expected)
        data = json.loads(response.content)
        expected = {'detail': 'No encontrado.'}
        self.assertEqual(data, expected)


    def test_200_language_filter_en(self):
        Project.objects.create(
            name="Test",
            cover_page="cover_page/image.jpg",
            description="ok",
            website="http://kalwak.ccr",
            date=timezone.now(),
            lan="en",
            pk=4
        )
        headers = {
            'Accept-Language': "en",
        }
        path = '/api/project/4/'
        response = self.client.get(path, **headers)
        expected = status.HTTP_200_OK
        self.assertEqual(response.status_code, expected)
        data = json.loads(response.content)["website"]
        expected = "http://kalwak.ccr"
        self.assertEqual(data, expected)

        response = self.client.get(path)
        expected = status.HTTP_200_OK
        self.assertEqual(response.status_code, expected)
        data = json.loads(response.content)["website"]
        expected = "http://kalwak.ccr"
        self.assertEqual(data, expected)

        path = '/api/project/1400/'
        response = self.client.get(path, **headers)
        expected = status.HTTP_404_NOT_FOUND
        self.assertEqual(response.status_code, expected)
        data = json.loads(response.content)
        expected = {'detail': 'Not found.'}
        self.assertEqual(data, expected)

    def test_400_language_filter(self):
        Project.objects.create(
            name="Test",
            cover_page="cover_page/image.jpg",
            description="ok",
            website="http://kalwak.cc",
            date=timezone.now(),
            lan="en",
            pk=5
        )
        headers = {
            'Accept-Language': "EN",
        }
        path = '/api/project/5/'
        response = self.client.get(path, **headers)
        expected = status.HTTP_200_OK
        self.assertEqual(response.status_code, expected)
        data = json.loads(response.content)["website"]
        expected = "http://kalwak.cc"
        self.assertEqual(data, expected)

        headers = {
            'Accept-Language': "dsakldas",
        }
        response = self.client.get(path, **headers)
        expected = status.HTTP_200_OK
        self.assertEqual(response.status_code, expected)
