from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from django.views.generic import TemplateView


class VuejsFrontend(TemplateView):
    """
    The default home page sent to vuejs
    """
    template_name = "index.html"


@csrf_protect
def vuejs_frontend404(request, exception=None):
    """
    This 404 goes to vuejs to be handled since we cannot bind an url from django directly to the vuejs Router.
    """
    return render(template_name="index.html", request=request)
