from django.test import TestCase
from rest_framework.test import APIClient
from Portfolio.settings import BASE_DIR
import os
import logging
log = logging.getLogger('debugger')


class LoggingTests(TestCase):

    def setUp(self) -> None:
        self.client = APIClient()
        self.error_path = os.path.join(BASE_DIR, 'logs/errors.log')
        self.debug_path = os.path.join(BASE_DIR, 'logs/debug.log')

    def test_debug_file_exists(self):
        self.assertEquals(os.path.exists(self.debug_path), True)

    def test_error_file_exists(self):
        self.assertEquals(os.path.exists(self.error_path), True)

