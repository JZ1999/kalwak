from unittest import skip

from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient


class LoggingTests(TestCase):
    def setUp(self) -> None:
        self.client = APIClient()
        User.objects.create_user(
            username="admin", password="admin12345", email="sample@gd.com"
        )

    @skip(reason="reCaptcha needs to be generated and sent")
    def test_get_jwt_token(self):
        response = self.client.post(
            "/api/auth/", data={"username": "admin", "password": "admin12345"}
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.data["token"])

    @skip(reason="reCaptcha needs to be generated and sent")
    def test_get_jwt_token_errors(self):
        response = self.client.post("/api/auth/", data={"username": "admin"})

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = self.client.post(
            "/api/auth/", data={"username": "admin", "password": "12345"}
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(
            response.data,
            {"non_field_errors": ["Unable to log in with provided credentials."]},
        )
