"""Portfolio URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, re_path
import environ
from django.views.generic import TemplateView
from rest_framework_jwt.views import obtain_jwt_token

from Service.views import JWTAuth
from frontend.views import vuejs_frontend404

env = environ.Env(
    # default value
    ADMIN_URL=(str, "admin/")
)

urlpatterns = [
    path(env("ADMIN_URL"), admin.site.urls),
    path("api/", include("Email.urls", namespace="email")),
    path("api/", include("Service.urls", namespace="service")),
    path("api/", include("Project.urls", namespace="project")),
    path("api/", include("Blog.urls", namespace="blog")),
    path("api/auth/", JWTAuth.as_view()),
    path("", include("frontend.urls", namespace="frontend")),
    path(
        "robots.txt",
        TemplateView.as_view(template_name="robots.txt", content_type="text/plain"),
    ),
    path(
        "sitemap.xml",
        TemplateView.as_view(template_name="sitemap.xml", content_type="text/xml"),
    ),
]
if settings.DEBUG:
    urlpatterns.append(path("api/", include("rest_framework.urls")))
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    # uncomment if frontend404 needs to be tested
    # urlpatterns.append(re_path(r'.*', vuejs_frontend404))

admin.site.site_header = "Kalwak Administration"
handler404 = "frontend.views.vuejs_frontend404"
