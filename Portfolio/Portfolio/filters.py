from rest_framework import filters
from Service import utils


class LanguageFilter(filters.BaseFilterBackend):
    """
    Filter that only allows users to see their own objects.
    """

    def filter_queryset(self, request, queryset, view):
        lan = utils.get_language_header(request)
        queryset = queryset.filter(lan=lan)
        return queryset
