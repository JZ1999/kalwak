import os
import re
from html5print import HTMLBeautifier
import sys

BASE_DIR = os.getcwd()


def mov_to_static_and_templates():
    static_path = BASE_DIR + "/frontend/static"
    index_path = BASE_DIR + "/frontend/templates/index.html"
    dist_path = BASE_DIR + "/dist"

    # Removing old files
    os.system(f"rm -rf {static_path}/ && rm {index_path}")

    # Moving new static files
    os.system(
        f"mv {dist_path}/static {static_path} && mv {dist_path}/favicon {static_path}"
    )

    # Moving new index.html
    os.system(f"mv {dist_path}/index.html {index_path}")

    # Moving robots.txt
    os.system(f"mv {dist_path}/robots.txt {BASE_DIR}/templates")

    # Moving sitemap.xml
    os.system(f"mv {dist_path}/sitemap.xml {BASE_DIR}/templates")

    # Removing dist
    os.system(f"rm -rf {dist_path}")


def pretty_html(file_path):
    with open(file_path, "r", encoding="utf-8") as file:
        html = file.read()
        pretty_file_content = HTMLBeautifier.beautify(html, 4)

    return pretty_file_content


def modify_index_html(pretty_file_content: str):
    new_file_content = ""
    for line in pretty_file_content.split("\n"):
        line += "\n"
        if "./favicon" in line:
            new_file_content += line.replace("./favicon", "/static/favicon")
        elif "!DOCTYPE" in line:
            new_file_content += line + "\n{% load i18n %}"
        elif "lang=" in line:
            ini = line.find("=")
            end = line.find(">")
            new_file_content += (
                "{% get_current_language as LANGUAGE_CODE %}"
                + line[: ini + 1]
                + "{{ LANGUAGE_CODE }}"
                + line[end:]
            )
        elif "meta" in line and (
            "description" in line or "keywords" in line or "title" in line
        ):
            ini = line.find('content="') + len('content="')
            end = line.find("name=")

            if end == -1:
                end = line.find("property=")

            new_file_content += (
                line[:ini] + "{% trans '" + line[ini : end - 2] + "' %}\" " + line[end:]
            )
        elif "Kalwak | Desarrollo web" in line:
            ini = line.find("K")
            end = line.find("web") + len("web")
            new_file_content += "{% trans '" + line[ini:end] + "' %}\n"
        elif (
            "Lo sentimos esta p&aacute;gina web no funciona sin JavaScript habilitado. Porfavor habilitelo"
            in line
        ):
            ini = line.find("L")
            end = line.find("habilitelo") + len("habilitelo")
            new_file_content += "{% trans '" + line[ini:end] + "' %}\n"

        else:
            new_file_content += line
    return new_file_content


def save_new_content(file_path, new_file_content):
    with open(file_path, "w", encoding="utf-8") as file:
        file.write(new_file_content)


def main():
    if len(sys.argv) <= 0:
        raise ValueError("A argument is needed")
    mov_to_static_and_templates()

    file_path = sys.argv[1]
    pretty_file_content = pretty_html(file_path)
    new_file_content = modify_index_html(pretty_file_content)

    save_new_content(file_path, new_file_content)


if __name__ == "__main__":
    main()
