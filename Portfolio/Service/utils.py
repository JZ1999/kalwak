from django.http import HttpResponseBadRequest
from rest_framework import status
from rest_framework.response import Response
from django.utils.translation import activate
from Portfolio import settings
from Service.serializers import FileSerializer, ServiceSerializer


def save_files(files, pk):
    """
    A helper function to save the files given in a form.
    :param files: a list of files or an empty string
    :param pk: the primary key of the service that the files are associated to
    :return: Nothing if successful or a rest_framework.response.Response with the errors of the serializer
    """
    data = []
    if (
        len(files) and files[0] != ""
    ):  # Must be done, since posting a form with no files defaults the file's value to ''
        for element in files:
            data.append({"service": pk, "file": element})
    if len(data):
        files = FileSerializer(data=data, many=True)
        if files.is_valid():
            files.save()
        else:
            return Response(files.errors, status=status.HTTP_400_BAD_REQUEST)


def save_services(services, pk):
    """
    A helper function to create the services and do error checking.
    :param services: A list of the services provided
    :param pk: The primary key of the Service
    :return: Nothing if successful or rest_framework.response.Response with the errors of the serializer
    """
    data = []
    for element in services:
        data.append({"service_request": pk, "service": element})
    if len(data):
        services = ServiceSerializer(data=data, many=True)
        if services.is_valid():
            services.save()
        else:
            return Response(services.errors, status=status.HTTP_400_BAD_REQUEST)


def get_language_header(request):
    lan = request.META.get("Accept-Language")
    # Needed because of unittests
    if not lan:
        lan = request.META.get("HTTP_ACCEPT_LANGUAGE")
        if not lan:
            lan = request.META.get("ACCEPT_LANGUAGE", settings.LANGUAGE_CODE)

    lan = lan.lower()
    if lan not in ("en", "es"):
        lan = settings.LANGUAGE_CODE
    activate(lan)
    return lan
