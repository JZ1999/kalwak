import json
import os
from django.http import JsonResponse
from ipware import get_client_ip
from rest_framework.views import APIView
from rest_framework_jwt.views import JSONWebTokenAPIView
from rivescript import RiveScript

from . import utils
from .utils import save_services, save_files, get_language_header
from Portfolio import settings
from Portfolio.settings import BASE_DIR
from Service.serializers import ChatbotSerializer, JWTSerializer
from .models import ServiceRequest
from rest_framework import viewsets
from .serializers import ServiceRequestSerializer
from rest_framework import status
from rest_framework.response import Response
import logging

log = logging.getLogger("debugger")


class JWTAuth(JSONWebTokenAPIView):
    serializer_class = JWTSerializer


class ServiceRequestView(viewsets.ModelViewSet):
    serializer_class = ServiceRequestSerializer
    queryset = ServiceRequest.objects.all()

    def create(self, request, *args, **kwargs):
        """
        Api view that saves a ServiceRequest and internally sends an email
        to website owners. To see this implementation go to:
        :func:`Service.serializers.ServiceRequestSerializer.create`

        :params request: should contain a JSON with name, email, description.
                        Also, can have attached several files.
        """
        # Activating correct language
        get_language_header(request)

        mutable = request.POST._mutable

        request.POST._mutable = True
        files = request.data.pop("files", [])
        request.FILES.clear()
        services = request.data.pop("services", [])

        request.POST._mutable = mutable
        serializer = self.serializer_class(
            data=request.data, context={"request": request}
        )

        if serializer.is_valid():
            instance = serializer.save()
            serializer.send_email(
                validated_data=serializer.validated_data, services=services
            )

            error = save_files(files, instance.pk)
            if error:
                return error
            error = save_services(services, instance.pk)
            if error:
                return error

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ChatbotAPIView(APIView):
    """
    An API that only accepts post data and sends it to the rivescript kalwak bot to be processed. It is meant to
    be displayed in the kalwak frontend as a chatbot.
    """

    serializer_class = ChatbotSerializer
    default_file = "user_data.json"
    rs = None  # Rivescript object

    def __init__(self):
        super(ChatbotAPIView, self).__init__()

    def _load_user_data(self, lan):
        """
        Gets the user_data for Rivescript if available in the file `user_data.json` and saves it to self.rs
        """
        try:
            with open(lan + "_" + self.default_file, "r") as user_data:
                size = os.path.getsize(user_data.name)
                # If the file weighs more then 200KB then reset it
                if size > 200_000:
                    raise TypeError  # Not actually TypeError, just used for convenience
                self.rs.set_uservars(json.loads(user_data.read()))
        except (FileNotFoundError, TypeError):
            with open(lan + "_" + self.default_file, "w") as file:
                file.write("{}")
        except Exception as e:
            log.error(f"_load_user_data Error {str(e)}")

    def _save_user_data(self, lan):
        """
        Saves the user_data for Rivescript  in the file `user_data.json`
        """
        try:
            with open(lan + "_" + self.default_file, "r") as file:
                old_user_data = json.loads(file.read())

            with open(lan + "_" + self.default_file, "w") as file:
                new_user_data = self.rs.get_uservars()
                old_user_data.update(new_user_data)
                file.write(json.dumps(old_user_data))
        except Exception as e:
            log.error(f"_save_user_data Error {str(e)}")

    def post(self, request):
        """
        When the post method is called we must load the rivescript chatbot scripts and save it to an attribute. This
        has to be done in the init otherwise user data will not be saved to the bot, such as the user's name.
        This api view is used to get input from the user and transform it accordingly using rivescript and return it.
        It must receive json data as follows:

        msg: the message you want to send to the rivescript kalwak bot,
        ip: an identifier for the kalwak bot to use

        :return: The kalwak bot response or the serializer errors
        """
        lan = utils.get_language_header(request)
        try:
            self.rs = RiveScript(utf8=True, debug=settings.DEBUG_CHATBOT)
            path = os.path.join(BASE_DIR, "chatbot_scripts/" + lan)
            self.rs.load_directory(path)
            self.rs.sort_replies()
            self._load_user_data(lan)
        except Exception as e:
            log.error(str(e))

        if not request.data:  # No data was sent, so send a default response
            desc = self.rs.get_variable(name="desc")
            return Response(desc)

        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            msg = request.POST.get("msg") or request.data.get("msg")
            ip = request.POST.get("ip") or request.data.get("ip")
            anchor_href = request.data.get("current_vue_path") + "#contact-section"
            self.rs.set_variable(name="anchor_contact_us_url", value=anchor_href)
            reply = self.rs.reply(ip, msg)

            self._save_user_data(lan)
            return Response(reply)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GetIP(APIView):
    """
    Return the ip of the client to be used with the rivescript chatbot, exclusively as a GET request
    """

    def get(self, request):
        ip = get_client_ip(request)[0]
        response = {"ip": ip}
        return JsonResponse(response)
