from rest_framework import serializers
from rest_framework_jwt.serializers import JSONWebTokenSerializer
from rest_framework_recaptcha.fields import ReCaptchaField

from Service.models import Service
from .models import ServiceRequest as ServiceR
from .models import File
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
import logging

log = logging.getLogger("debugger")


class JWTSerializer(JSONWebTokenSerializer):
    recaptchakey = ReCaptchaField(
        error_messages={"invalid-input-response": "reCAPTCHA token is invalid.",}
    )


class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = "__all__"


class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = "__all__"


class ServiceRequestSerializer(serializers.HyperlinkedModelSerializer):
    files = FileSerializer(many=True, required=False)
    services = ServiceSerializer(many=True, required=False)
    recaptcha_key = ReCaptchaField(
        error_messages={"invalid-input-response": "reCAPTCHA token is invalid."}
    )

    class Meta:
        model = ServiceR
        fields = (
            "name",
            "email",
            "telephone",
            "description",
            "files",
            "services",
            "recaptcha_key",
        )

    def create(self, validated_data: dict):
        """
        Create method overrided to save several files from the same request
        and send an email to website owners.
        """
        validated_data.pop("recaptcha_key")
        service = ServiceR.objects.create(**validated_data)

        return service

    @staticmethod
    def send_email(validated_data: dict, services: list):
        if services:
            validated_data["services"] = services[0].split(",")
        validated_data["subject"] = "Cotización"
        html_email = render_to_string("hire_us_email.html", validated_data)
        plain_email = strip_tags(html_email)
        to_email = settings.CONTACT_EMAIL
        email = EmailMultiAlternatives(
            "Kalwak: Service Request", plain_email, to_email, [to_email]
        )
        email.attach_alternative(html_email, "text/html")

        response = email.send()
        return response


class ChatbotSerializer(serializers.Serializer):
    """
    Does not need to override the update or create methods since this is used only to validate that the JSON sent to the
    ChatbotAPIView is valid.
    """

    msg = serializers.CharField()
    ip = serializers.IPAddressField()
    regex = r"(\/[0-9].*\?|$)"
    current_vue_path = serializers.RegexField(regex=regex)
