from unittest import skip

from django.test import TestCase, override_settings
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status

from Portfolio import settings
from Portfolio.settings import BASE_DIR
from Service.models import File, ServiceRequest, Service
import os


class ServiceTestCase(TestCase):
    def setUp(self) -> None:
        self.client = APIClient()
        self.file_path = os.path.join(BASE_DIR, "Service/tests/files/test_file.txt")
        self.file_pdf_path = os.path.join(BASE_DIR, "Service/tests/files/test.pdf")

    def test_service_get(self):
        response = self.client.get("/api/service_request/")
        status_expected = status.HTTP_405_METHOD_NOT_ALLOWED
        self.assertEqual(response.status_code, status_expected)

    @override_settings(THROTTLE_ANON_TRESHOLD=5)
    def test_throttle_service_request(self):
        # some end point you want to test (in this case it's a public enpoint that doesn't require authentication
        _url = reverse("service:service_request")
        # this is probably set in settings in you case
        for i in range(5):
            self.client.get(_url)

        # this call should err
        response = self.client.post(_url)
        # 429 - too many requests
        self.assertEqual(response.status_code, 429)

    def test_service_request_post_successful(self):
        data = {
            "name": "Fooname",
            "email": "fooo@gmail.com",
            "description": "foodescription",
            "telephone": "84599021",
        }
        response = self.client.post("/api/service_request/", data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response.data.pop("files")
        response.data.pop("services")
        self.assertEqual(response.data, data)

    def test_service_request_services_post_successful(self):
        data = {
            "name": "Fooname",
            "email": "fooo@gmail.com",
            "description": "foodescription",
            "telephone": "84599021",
            "services": ["test"],
        }
        response = self.client.post("/api/service_request/", data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        service = Service.objects.last()
        self.assertEqual(service.service, data["services"][0])

    def test_service_request_wrong_email(self):
        data = {
            "name": "Fooname",
            "email": "fooo",
            "description": "foodescription",
            "telephone": "84599021",
        }
        response = self.client.post("/api/service_request/", data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_service_request_name_empty(self):
        data = {
            "name": "",
            "email": "fooo@gmail.com",
            "description": "foodescription",
            "telephone": "84599021",
        }
        response = self.client.post("/api/service_request/", data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_service_request_description_empty(self):
        data = {
            "name": "Fooname",
            "email": "fooo@gmail.com",
            "description": "",
            "telephone": "84599021",
        }
        response = self.client.post("/api/service_request/", data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_service_request_missing_email_field(self):
        data = {
            "name": "FooName",
            "description": "foodescription",
            "telephone": "84599023",
        }
        response = self.client.post("/api/service_request/", data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_service_request_missing_name_field(self):
        data = {
            "email": "fooo@gmail.com",
            "description": "foodescription",
            "telephone": "84599023",
        }
        response = self.client.post("/api/service_request/", data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_service_request_missing_description_field(self):
        data = {"name": "Fooname", "email": "fooo@gmail.com", "telephone": "84599021"}
        response = self.client.post("/api/service_request/", data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_service_request_saving_file(self):
        data = {
            "name": "Fooname",
            "email": "fooo1@gmail.com",
            "description": "foodescription",
            "telephone": "84599021",
            "files": open(self.file_path, "rb"),
        }
        response = self.client.post(
            "/api/service_request/", data=data, format="multipart"
        )
        data.pop("files")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        saved_file = File.objects.last()
        self.assertEqual(saved_file.service.email, "fooo1@gmail.com")
        saved_file.service.delete()
        saved_file.delete()

    def test_service_request_saving_several_files(self):
        data = {
            "name": "Fooname",
            "email": "fooo@gmail.com",
            "description": "foodescription",
            "telephone": "84599021",
            "files": [open(self.file_path, "rb"), open(self.file_pdf_path, "rb")],
        }
        response = self.client.post(
            "/api/service_request/", data=data, format="multipart"
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        saved_file = File.objects.last()
        self.assertEqual(saved_file.service.email, "fooo@gmail.com")
        saved_file.delete()
        saved_file = File.objects.last()
        self.assertEqual(saved_file.service.email, "fooo@gmail.com")
        saved_file.service.delete()
        saved_file.delete()
