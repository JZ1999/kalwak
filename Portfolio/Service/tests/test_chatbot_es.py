import json
import os
import re
from ipaddress import IPv4Address
from random import getrandbits

import requests
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from Service.views import ChatbotAPIView


class ChatbotSpanishTests(TestCase):
    url = ""

    def generate_random_ip(self) -> str:
        bits = getrandbits(32)  # generates an integer with 32 random bits
        addr = IPv4Address(bits)  # instances an IPv4Address object from those bits
        addr_str = str(addr)  # get the IPv4Address object's string representation
        return addr_str

    def remove_extra_double_quotes(self, string: str) -> str:
        if string[0] == '"':
            string = string[1:]
        if string[-1] == '"':
            string = string[: len(string) - 1]
            if string[-1] == "\\":
                string = string[: len(string) - 1]

        return string

    def setUp(self) -> None:
        self.client = APIClient(ACCEPT_LANGUAGE="es")
        self.client.defaults.update({"Accept-Language": "es"})
        self.url = reverse("service:chatbot")

    def tearDown(self) -> None:
        os.system(f"rm es_{ChatbotAPIView.default_file}")

    def test_wrong_accepted_language(self):
        ip = self.generate_random_ip()
        data = {"msg": "Cual es mi nombre", "ip": ip, "current_vue_path": "/"}
        headers = {"Accept-Language": "ll"}
        response = self.client.post(self.url, data=data, **headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_ip_generator(self):
        ip = self.generate_random_ip()
        regex = re.compile(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
        match = regex.match(ip)
        self.assertTrue(match is not None)

    def test_400_msg(self):
        data = {"msg": "Hola", "ip": "1"}
        response = self.client.post(self.url, data=data)
        response = json.loads(response.content)
        expected_status = 400
        expected = "Introduzca una dirección IPv4 o IPv6 válida."
        self.assertEqual(expected, response["ip"][0])
        self.assertEqual(expected_status, status.HTTP_400_BAD_REQUEST)

    def test_400_required(self):
        data = {"msg": "Hola"}
        response = self.client.post(self.url, data=data)
        response = json.loads(response.content)
        expected_status = 400
        expected = "Este campo es requerido."
        self.assertEqual(expected, response["ip"][0])
        self.assertEqual(expected_status, status.HTTP_400_BAD_REQUEST)

        data = {"ip": self.generate_random_ip()}
        response = self.client.post(self.url, data=data)
        response = json.loads(response.content)
        expected_status = 400
        expected = "Este campo es requerido."
        self.assertEqual(expected, response["msg"][0])
        self.assertEqual(expected_status, status.HTTP_400_BAD_REQUEST)

    def test_get_put_delete_patch_methods(self):
        expected_status = 405

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, expected_status)

        response = self.client.put(self.url)
        self.assertEqual(response.status_code, expected_status)

        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, expected_status)

        response = self.client.patch(self.url)
        self.assertEqual(response.status_code, expected_status)

    def test_chatbot_output1(self):
        ip = self.generate_random_ip()
        data = {"msg": "Cual es mi nombre", "ip": ip, "current_vue_path": "/"}
        response = self.client.post(self.url, data=data)
        response = response.content.decode().replace('"', "")
        expected = "Nunca me dijiste tu nombre."
        self.assertEqual(response, expected)

        data = {"msg": "Mi nombre es Jose", "ip": ip, "current_vue_path": "/"}
        response = self.client.post(self.url, data=data)
        response = response.content.decode().replace('"', "")
        expected = ["Jose, encantado de conocerte.", "Encantado de conocerte, Jose."]
        self.assertIn(response, expected)

        data = {"msg": "Mi nombre es Joseph", "ip": ip, "current_vue_path": "/"}
        response = self.client.post(self.url, data=data)
        response = response.content.decode().replace('"', "")
        expected = "Ese también es el nombre de mi maestro."
        self.assertIn(response, expected)

        data = {"msg": "CUAL ES MI NOMBRE??", "ip": ip, "current_vue_path": "/"}
        response = self.client.post(self.url, data=data)
        response = response.content.decode().replace('"', "")
        expected = [
            "Tu nombre es Joseph.",
            "Me dijiste que tu nombre es Joseph.",
            "¿No eres Joseph?",
        ]
        self.assertIn(response, expected)

    def test_chatbot_output2(self):
        ip = self.generate_random_ip()
        data = {"msg": "RANDOM TESTING TEXT", "ip": ip, "current_vue_path": "/"}
        response = self.client.post(self.url, data=data)
        response = response.content.decode().replace('"', "")
        expected = [
            "Lo siento, no entiendo.",
            "No lo entendí del todo.",
            "No estoy seguro de entenderte completamente.",
            "Continúa por favor.",
            "Eso es interesante. Por favor continua.",
            "Cuéntame más sobre eso.",
            "¿Te molesta hablar de esto?",
        ]
        self.assertIn(response, expected)

        data = {"msg": "Mi NOmBre eS Jose", "ip": ip, "current_vue_path": "/"}
        response = self.client.post(self.url, data=data)
        response = response.content.decode().replace('"', "")
        expected = ["Jose, encantado de conocerte.", "Encantado de conocerte, Jose."]
        self.assertIn(response, expected)

    def test_chatbot_output3(self):
        ip = self.generate_random_ip()
        data = {"msg": "Hola", "ip": ip, "current_vue_path": "/"}
        response = self.client.post(self.url, data=data)
        response = response.content.decode().replace('"', "")
        expected = "Saludos"
        self.assertTrue(expected in response)

        data = {"msg": "Edwin", "ip": ip, "current_vue_path": "/"}
        response = self.client.post(self.url, data=data)
        response = response.content.decode().replace('"', "")
        expected = ["Edwin, encantado de conocerte.", "Encantado de conocerte, Edwin."]
        self.assertIn(response, expected)

    def test_chatbot_output4(self):
        ip2 = self.generate_random_ip()
        data = {"msg": "Hola", "ip": ip2, "current_vue_path": "/"}
        response = self.client.post(self.url, data=data)
        response = response.content.decode().replace('"', "")
        expected = "Saludos"
        self.assertTrue(expected in response)

        data = {"msg": "Edwin", "ip": ip2, "current_vue_path": "/"}
        response = self.client.post(self.url, data=data)
        response = response.content.decode().replace('"', "")
        expected = ["Edwin, encantado de conocerte.", "Encantado de conocerte, Edwin."]
        self.assertIn(response, expected)

        ip1 = self.generate_random_ip()
        data = {"msg": "Me llamo Jennifer", "ip": ip1, "current_vue_path": "/"}
        response = self.client.post(self.url, data=data)
        response = response.content.decode().replace('"', "")
        expected = "Jennifer, te llamaré así a partir de ahora."
        self.assertEqual(response, expected)

        expected = "Jennifer"
        data = {"msg": "quien soy?", "ip": ip1, "current_vue_path": "/"}
        response = self.client.post(self.url, data=data)
        response = response.content.decode().replace('"', "")
        self.assertIn(expected, response)

        expected = "Edwin"
        data = {"msg": "quien soy?", "ip": ip2, "current_vue_path": "/"}
        response = self.client.post(self.url, data=data)
        response = response.content.decode().replace('"', "")
        self.assertIn(expected, response)

    def test_chatbot_output5(self):
        ip = self.generate_random_ip()
        data = {"msg": "Cuales son tus servicios?", "ip": ip, "current_vue_path": "/"}
        response = self.client.post(self.url, data=data)
        response = response.content.decode().replace('"', "")
        expected = "Servicios </a>"

        self.assertIn(expected, response)

    def test_chatbot_output6(self):
        ip = self.generate_random_ip()
        data = {
            "msg": "Que nos ofrecen en desarrollo web??",
            "ip": ip,
            "current_vue_path": "/",
        }
        response = self.client.post(self.url, data=data)
        response = response.content.decode()
        expected = "Servicios </a>"

        self.assertIn(expected, response)

    def test_chatbot_output_test_links(self):
        ip = self.generate_random_ip()
        data = {"msg": "facebook", "ip": ip, "current_vue_path": "/"}
        response = self.client.post(self.url, data=data)
        response = response.content.decode()
        response = self.remove_extra_double_quotes(response)

        regex = r"\".*\"> facebook"
        match = re.findall(regex, response)[0]
        match = match.replace("> facebook", "")
        match = self.remove_extra_double_quotes(match)

        result = requests.get(match)

        expected = 200

        self.assertEqual(expected, result.status_code)
        data = {
            "msg": "cuales son los productos que venden?",
            "ip": ip,
            "current_vue_path": "/",
        }
        response = self.client.post(self.url, data=data)
        response = response.content.decode()
        response = self.remove_extra_double_quotes(response)

        match = re.findall(regex, response)[0]
        match = match.replace("> facebook", "")
        match = self.remove_extra_double_quotes(match)

        result = requests.get(match)

        expected = 200
        self.assertEqual(expected, result.status_code)

    def test_chatbot_output7(self):
        ip = self.generate_random_ip()
        data = {
            "msg": "Dame UNA manera de contactarme con tus desarrolladores!",
            "ip": ip,
            "current_vue_path": "/path1/",
        }
        response = self.client.post(self.url, data=data)
        response = response.content.decode()
        response = self.remove_extra_double_quotes(response)
        expected = "Envíe un mensaje a <a href="

        self.assertIn(expected, response)

    def test_chatbot_output8(self):
        ip = self.generate_random_ip()
        data = {"msg": "info", "ip": ip, "current_vue_path": "/"}
        response = self.client.post(self.url, data=data)
        response = response.content.decode().replace('"', "")
        expected = "Kalwak es una compañía "
        self.assertIn(expected, response)

    def test_chatbot_output9(self):
        ip = self.generate_random_ip()
        data = {"msg": "dime sobre el equipo", "ip": ip, "current_vue_path": "/"}
        response = self.client.post(self.url, data=data)
        response = response.content.decode().replace('"', "")
        expected = "El equipo está formado por profesionales"
        self.assertIn(expected, response)
