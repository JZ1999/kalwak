Service package
===============

Subpackages
-----------

.. toctree::

   Service.tests

Submodules
----------

Service.admin module
--------------------

.. automodule:: Service.admin
   :members:
   :undoc-members:
   :show-inheritance:

Service.apps module
-------------------

.. automodule:: Service.apps
   :members:
   :undoc-members:
   :show-inheritance:

Service.models module
---------------------

.. automodule:: Service.models
   :members:
   :undoc-members:
   :show-inheritance:

Service.serializers module
--------------------------

.. automodule:: Service.serializers
   :members:
   :undoc-members:
   :show-inheritance:

Service.urls module
-------------------

.. automodule:: Service.urls
   :members:
   :undoc-members:
   :show-inheritance:

Service.views module
--------------------

.. automodule:: Service.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: Service
   :members:
   :undoc-members:
   :show-inheritance:
