Project package
===============

Subpackages
-----------

.. toctree::

   Project.tests

Submodules
----------

Project.admin module
--------------------

.. automodule:: Project.admin
   :members:
   :undoc-members:
   :show-inheritance:

Project.apps module
-------------------

.. automodule:: Project.apps
   :members:
   :undoc-members:
   :show-inheritance:

Project.middleware module
-------------------------

.. automodule:: Project.middleware
   :members:
   :undoc-members:
   :show-inheritance:

Project.models module
---------------------

.. automodule:: Project.models
   :members:
   :undoc-members:
   :show-inheritance:

Project.serializers module
--------------------------

.. automodule:: Project.serializers
   :members:
   :undoc-members:
   :show-inheritance:

Project.urls module
-------------------

.. automodule:: Project.urls
   :members:
   :undoc-members:
   :show-inheritance:

Project.views module
--------------------

.. automodule:: Project.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: Project
   :members:
   :undoc-members:
   :show-inheritance:
