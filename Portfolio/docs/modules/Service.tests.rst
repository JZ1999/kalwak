Service.tests package
=====================

Submodules
----------

Service.tests.test\_chatbot module
----------------------------------

.. automodule:: Service.tests.test_chatbot
   :members:
   :undoc-members:
   :show-inheritance:

Service.tests.test\_service\_views module
-----------------------------------------

.. automodule:: Service.tests.test_service_views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: Service.tests
   :members:
   :undoc-members:
   :show-inheritance:
