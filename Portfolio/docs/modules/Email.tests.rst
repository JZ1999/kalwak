Email.tests package
===================

Submodules
----------

Email.tests.test\_email\_views module
-------------------------------------

.. automodule:: Email.tests.test_email_views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: Email.tests
   :members:
   :undoc-members:
   :show-inheritance:
