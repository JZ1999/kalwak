frontend package
================


Submodules
----------

frontend.urls module
--------------------

.. automodule:: frontend.urls
   :members:
   :undoc-members:
   :show-inheritance:

frontend.views module
---------------------

.. automodule:: frontend.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: frontend
   :members:
   :undoc-members:
   :show-inheritance:
