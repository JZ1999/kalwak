Email package
=============

Subpackages
-----------

.. toctree::

   Email.tests

Submodules
----------

Email.apps module
-----------------

.. automodule:: Email.apps
   :members:
   :undoc-members:
   :show-inheritance:

Email.serializers module
------------------------

.. automodule:: Email.serializers
   :members:
   :undoc-members:
   :show-inheritance:

Email.urls module
-----------------

.. automodule:: Email.urls
   :members:
   :undoc-members:
   :show-inheritance:

Email.views module
------------------

.. automodule:: Email.views
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: Email
   :members:
   :undoc-members:
   :show-inheritance:
