.. Kalwak_Portfolio documentation master file, created by
   sphinx-quickstart on Sat Aug 24 11:37:07 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Kalwak_Portfolio's documentation!
============================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:
   
   modules/modules.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
