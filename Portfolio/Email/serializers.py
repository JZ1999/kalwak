from rest_framework import serializers
from django.core.mail import EmailMessage, send_mail
from django.conf import settings
import logging

from rest_framework_recaptcha.fields import ReCaptchaField
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from django.template import loader

log = logging.getLogger("debugger")


class EmailSerializer(serializers.Serializer):
    subject = serializers.CharField(max_length=50, allow_blank=False)
    email = serializers.EmailField(allow_blank=False)
    message = serializers.CharField(max_length=1000, allow_blank=False)
    phone_number = serializers.CharField(max_length=1000, allow_blank=False)
    name = serializers.CharField(max_length=1000, allow_blank=False)
    recaptcha_key = ReCaptchaField(
        error_messages={"invalid-input-response": "reCAPTCHA token is invalid."}
    )

    def send_email(self, request) -> int:
        """
        Sends email using serialized data.
        Returns 1 if the operation is successful and 0 if it fails.
        """
        from_email = settings.SERVER_EMAIL
        to_email = settings.CONTACT_EMAIL

        msg = self.data["message"]
        email = self.data["email"]
        number = self.data["phone_number"]
        name = self.data["name"]
        subject = self.data["subject"]

        render = self.render_email(
            msg=msg,
            subject=subject,
            user_email=email,
            number=number,
            name=name,
            request=request,
        )

        message = Mail(
            from_email=from_email,
            to_emails=to_email,
            subject=subject,
            html_content=render,
        )
        try:
            sg = SendGridAPIClient(settings.EMAIL_HOST_PASSWORD)
            if not settings.SENDGRID_SANDBOX_MODE_IN_DEBUG:
                response = sg.send(message)
            else:
                return 1
        except Exception as e:
            return 0
        if response.status_code not in range(200, 300):
            return 0
        return 1

    @staticmethod
    def render_email(
        *, msg: str, subject: str, user_email: str, number: str, name: str, request
    ):
        template = loader.get_template("contact_us_email.html")
        context = {
            "subject": subject,
            "description": msg,
            "user_email": user_email,
            "number": number,
            "name": name,
        }
        email = template.render(context, request)
        return email
