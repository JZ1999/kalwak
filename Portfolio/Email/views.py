from .serializers import EmailSerializer
from rest_framework import viewsets, status
from rest_framework.response import Response
import logging

log = logging.getLogger("debugger")


class SendEmailView(viewsets.ViewSet):
    serializer_class = EmailSerializer

    def create(self, request):
        """
        Post Method that sends an Email from the 'Contact us' page form
        to the group Email.
        :params request: should contain a JSON with email,subject and message
        """
        serializer = EmailSerializer(data=request.data)
        if serializer.is_valid():
            response = serializer.send_email(request=request)
            if response == 1:
                status_code = status.HTTP_200_OK
            else:
                status_code = status.HTTP_503_SERVICE_UNAVAILABLE
            return Response({"response": response}, status=status_code)
        return Response(serializer.errors)
